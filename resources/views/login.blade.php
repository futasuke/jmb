<html>
  <head>
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = {{ asset("css/bootstrap.css") }} rel="stylesheet" />
    <link href = {{ asset("css/bootstrap-reboot.css") }} rel="stylesheet" />
    <link href = {{ asset("css/bootstrap-grid.css") }} rel="stylesheet" />
    <link href = {{ asset("css/login/login.css") }} rel="stylesheet" />
  </head>
  <body>
    @if (session()->has('error_message'))
      <div class="message">
          <ul>
            <li>{{ session('error_message') }}</li>
          </ul>
      </div>
    @endif
    <div class="loginbox">
      <img src="{{asset('/picture/avatar.png')}}" class="avatar">
      <h1>Login Here</h1>
      <form method="post" action="{{url('/request-login')}}">
        @csrf
        <p>Email</p>
        <input type="text" name="email" placeholder="Enter Email" required>
        <p>Password</p>
        <input type="password" name="password" placeholder="Enter Password" required>
        <input type="submit" value="Login">
      </form>
    </div>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
  </body>
</html>
