@extends('officer.layout.master')
@section('title','Owner List')

@section('custom_css')
<link href = {{ asset("css/datatable/showDataTable.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_showOwner') }}
  </div>

  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <div id="inner-content">
    <h1 style="margin-bottom:50px">Owner List Section</h1>

    <table id="owner" class="display">
        <thead>
            <tr>
                <th>Username</th>
                <th>Full Name</th> 
                <th>Email</th>
                <th>Address</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Delete</h4>
          </div>
          <div class="modal-body">
            <p style="color:black;">Are you sure you want to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="submit-button" data-dismiss="modal">Close</button>
            <form method="POST" action="" style="margin:0px;" id="deleteForm">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input type="submit" class="submit-button" value="Delete">
            </form>
          </div>
        </div>
        
      </div>
    </div>
    <!-- Modal End -->
    
  </div>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#owner').DataTable({
      serverSide: true,
      ajax: "{{ route('data_officer_owner') }}",
      columns: [
        { data:'username', name: 'username'},
        { data:'user_info.fullname', name: 'user_info.fullname'},
        { data:'email', name: 'email'},
        { data:'user_info.address', name: 'user_info.address'},
        { data:'id', name: 'id'},
      ],
      columnDefs: [
        {
          targets: 4,
          "data": 'id',
          render: function ( data, type, row, meta ) {
            return '<a href="edit-officer-owner/'+data+'" class="far fa-edit submit-button"></a> 	&nbsp; <a href="#" class="far fa-trash-alt submit-button" onclick="deleteData('+data+')" data-toggle="modal" data-target="#myModal"></a>';
          }
        }
      ],
    });
} );

function deleteData(id){
  var id = id;
  var url = '{{url("/officer-delete-owner")}}/'+id;

  $('#deleteForm').attr('action',url);
}

</script>
@endsection
