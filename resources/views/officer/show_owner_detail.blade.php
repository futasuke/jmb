@extends('officer.layout.master')
@section('title','Owner Detail')

@section('custom_css')
<link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
<div>
    {{ Breadcrumbs::render('officer_editOwner', $ownerId) }}
</div>

@if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

<div id="inner-content">
    <h1 style="margin-bottom:50px">Detail's : {{$owner->UserInfo->fullname}}</h1>

    <table class="profile-table">
      <tr>
        <td><span>Fullname: </span></td>
        <td><span>{{$owner->UserInfo->fullname}}</span></td>
      </tr>
      <tr>
        <td><span>Username: </span></td>
        <td><span>{{$owner->username}}</span></td>
      </tr>
      <tr>
        <td><span>Email: </span></td>
        <td><span>{{$owner->email}}</span></td>
      </tr>
      <tr>
        <td><span>Phone: </span></td>
        <td><span>{{$owner->UserInfo->phone}}</span></td>
      </tr>
      <tr>
        <td><span>Address: </span></td>
        <td><span>{{$owner->UserInfo->address}}</span></td>
      </tr>
      <tr>
        <td><span>Owned Unit: <br> <p style="font-size:16;">*unit can be clicked<p> </span></td>
        <td>
          <ul>
            @foreach($owner->Unit as $unit)
            <li>
              <a href="{{url('/officer-detail-unit')}}/{{$unit->id}}">{{$unit->number}}</a>
            </li>
            @endforeach
          </ul>
        </td>
      </tr>
      <tr> 
        <td>
          <a href="{{url('/officer-update-owner')}}/{{$owner->id}}" class="submit-button">Edit</a>
        </td>
      </tr>
    </table>

    <div class="straight-line"></div>
    <h3 style="margin-bottom:2%;">{{$owner->username}}'s Tenant List</h3>

    <table id="tenant" class="display">
        <thead>
            <tr>
                <th>Unit Number</th>
                <th>First Name</th>
                <th>Last Name</th> 
                <th>IC</th>
                <th>Citizenship</th>
                <th>Phone</th>
                <th>Stay Duration (month)</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#tenant').DataTable({
      serverSide: true,
      ajax: "{{ route('data_officer_tenant',$owner->id) }}",
      columns: [
        { data: 'number', name: 'number' },
        { data: 'tenant.fname', name: 'tenant.fname' },
        { data: 'tenant.lname', name: 'tenant.lname' },
        { data: 'tenant.ic', name: 'tenant.ic' },
        { data: 'tenant.citizenship', name: 'tenant.citizenship' },
        { data: 'tenant.phone', name: 'tenant.phone' },
        { data: 'tenant.duration', name: 'tenant.duration' },
      ],
    });
} );
</script>
@endsection