@extends('officer.layout.master')
@section('title','Profile')

@section('custom_css')
  <link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_profile') }}
  </div>

  <div id="inner-content">
    <h1 class="profile-title">{{ucfirst($user->username)}}'s profile</h1> <!-- ucfirst() make first character to uppercase-->
    <div class="row">
        <div class="col-xs-4 col-md-3 col-xl-3">
          <img src="{{asset('profilepic/daus_profile.jpg')}}" alt="Profile pic" class="profile-picture"><br>
        </div>
        <div class="col-md-9">
          <table class="profile-table">
            <tr>
              <td align="right"><span>Fullname : </span></td>
              <td>{{$user->UserInfo->fullname}}</td>
            </tr>
            <tr>
              <td align="right"><span>Email : </span></td>
              <td>{{$user->email}}</td>
            </tr>
            <tr>
              <td align="right"><span>Phone : </span></td>
              <td>0{{$user->UserInfo->phone}}</td>
            </tr>
            <tr>
              <td align="right"><span>Address : </span></td>
              <td>{{$user->UserInfo->address}}</td>
            </tr>
            <tr>
              <td align="right"><span>Role : </span></td>
              <td>{{$user->role}}</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>

          <div class="profile-btn-container">
            <a class="profile-update-btn" href="{{url('/officer-profile-edit')}}">Edit Profile</a>
          </div>
        </div>
    </div>
  </div>
@endsection

@section('custom_js')
@endsection
