@extends('officer.layout.master')
@section('title','Unit List')

@section('custom_css')
<link href = {{ asset("css/datatable/showDataTable.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
<div>
  {{ Breadcrumbs::render('officer_showUnit') }}
</div>

@if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

<div id="inner-content">
  <h1 style="margin-bottom:50px">All Unit List</h1>
  <div class="select-block">
    <span style="font-weight:bold;">Select Block: <span><select id="getBlock" style="margin-left:20px;" onChange="showDiv(this)">
                                <option value="blockA">Block A</option>
                                <option value="blockB">Block B</option>
                                <option value="blockC">Block C</option>
                              </select>
  </div>

  <div id="blockA" style="display:block">
    <table id="unitA" class="display">
      <thead>
          <tr>
              <th>Unit Number</th>
              <th>Level</th> 
              <th>Block</th>
              <th>Status</th>
              <th>Edit</th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <div id="blockB" style="display:none">
    <table id="unitB" class="display">
      <thead>
          <tr>
              <th>Unit Number</th>
              <th>Level</th> 
              <th>Block</th>
              <th>Status</th>
              <th>Edit</th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <div id="blockC" style="display:none;width:100%;">
    <table id="unitC" class="display">
      <thead>
          <tr>
              <th>Unit Number</th>
              <th>Level</th> 
              <th>Block</th>
              <th>Status</th>
              <th>Edit</th>
          </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Delete</h4>
          </div>
          <div class="modal-body">
            <p style="color:black;">Are you sure you want to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="submit-button" data-dismiss="modal">Close</button>
            <form method="POST" action="" style="margin:0px;" id="deleteForm">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input type="submit" class="submit-button" value="Delete">
            </form>
          </div>
        </div>
        
      </div>
  </div>
  <!-- Modal End -->
</div>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#unitA').DataTable({
      serverSide: true,
      ajax: "{{ url('/data-officer-unit/blockA') }}",
      columns: [
        { name: 'number' },
        { name: 'level' },
        { name: 'block' },
        { name: 'status' },
        { name: 'id' },
      ],
      columnDefs: [
        {
          targets: 4,
          "data": null,
          render: function ( data, type, row, meta ) {
            return '<a href="officer-detail-unit/'+row[4]+'" class="far fa-edit submit-button"></a> 	&nbsp; <a href="#" class="far fa-trash-alt submit-button" onclick="deleteData('+row[4]+')" data-toggle="modal" data-target="#myModal"></a>';
          }
        }
      ],
    });

    $('#unitB').DataTable({
      serverSide: true,
      ajax: "{{ url('/data-officer-unit/blockB') }}",
      columns: [
        { name: 'number' },
        { name: 'level' },
        { name: 'block' },
        { name: 'status' },
        { name: 'id' },
      ],
      columnDefs: [
        {
          targets: 4,
          "data": null,
          render: function ( data, type, row, meta ) {
            return '<a href="officer-detail-unit/'+row[4]+'" class="far fa-edit submit-button"></a> 	&nbsp; <a href="#" onclick="deleteData('+row[4]+')" data-toggle="modal" data-target="#myModal" class="far fa-trash-alt submit-button"></a>';
          }
        }
      ],
    });

    $('#unitC').DataTable({
      serverSide: true,
      ajax: "{{ url('/data-officer-unit/blockC') }}",
      columns: [
        { name: 'number' },
        { name: 'level' },
        { name: 'block' },
        { name: 'status' },
        { name: 'id' },
      ],
      columnDefs: [
        {
          targets: 4,
          "data": null,
          render: function ( data, type, row, meta ) {
            return '<a href="officer-detail-unit/'+row[4]+'" class="far fa-edit submit-button"></a> 	&nbsp; <a href="#" onclick="deleteData('+row[4]+')"  data-toggle="modal" data-target="#myModal" class="far fa-trash-alt submit-button"></a>';
          }
        }
      ],
    });
});

function showDiv(select) {
  if(select.value=='blockA'){
    document.getElementById('blockA').style.display = "contents";
    document.getElementById('blockB').style.display = "none";
    document.getElementById('blockC').style.display = "none";
  } 
  if (select.value=='blockB') {
    document.getElementById('blockA').style.display = "none";
    document.getElementById('blockB').style.display = "contents";
    document.getElementById('blockC').style.display = "none";
  }
  if (select.value=='blockC') {
    document.getElementById('blockA').style.display = "none";
    document.getElementById('blockB').style.display = "none";
    document.getElementById('blockC').style.display = "contents";   
  }
}

function deleteData(id){
  var id = id;
  var url = '{{url("/officer-delete-unit")}}/'+id;

  $('#deleteForm').attr('action',url);
}
</script>
@endsection
