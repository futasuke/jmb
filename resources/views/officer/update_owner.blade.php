@extends('officer.layout.master')
@section('title','Update Owner')

@section('custom_css')
<link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_home') }}
  </div>

  <!-- Show success or error message start -->
  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif
  <!-- Show success or error message end -->

  <!-- This is content -->
  <div id="inner-content">
    <h1 style="margin-bottom:50px">Detail's : {{$owner->UserInfo->fullname}}</h1>
    <form action="{{route('officer_requestUpdateOwner', $owner->id)}}" method="post">
        @csrf
        <table class="profile-table">
        <tr>
            <td><span>Fullname: </span></td>
            <td><input type="text" value="{{$owner->UserInfo->fullname}}" name="fullname"></td>
        </tr>
        <tr>
            <td><span>Username: </span></td>
            <td><input type="text" value="{{$owner->username}}" name="username"></td>
        </tr>
        <tr>
            <td><span>Email: </span></td>
            <td><input type="text" value="{{$owner->email}}" name="email"></td>
        </tr>
        <tr>
            <td><span>Phone: </span></td>
            <td><input type="text" value="{{$owner->UserInfo->phone}}" name="phone"></td>
        </tr>
        <tr>
            <td><span>Address: </span></td>
            <td><input type="text" value="{{$owner->UserInfo->address}}" name="address"></td>
        </tr>
        </table>
        <div class="button-container">
            <a href="{{route('officer_editOwner',$owner->id)}}" class="submit-button">Cancel</a>
            <input type="submit" class="submit-button" value="Update">
        </div>
    </form>
</div>
@endsection

@section('custom_js')
@endsection
