<html>
<head>
    <title>Officer Dashboard - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href={{ asset("css/bootstrap.css") }} rel="stylesheet" />
    <link href = {{ asset("css/datatables.css") }} rel="stylesheet" />
    <link href={{ asset("css/bootstrap-reboot.css") }} rel="stylesheet" />
    <link href={{ asset("css/bootstrap-grid.css") }} rel="stylesheet" />
    <link href={{ asset("css/style.css") }} rel="stylesheet" />
    <script src="https://kit.fontawesome.com/c92cae009f.js"></script>
    @yield('custom_css')
</head>

<body>
    <div class="wrapper" style="margin-left: 0px">

        <nav id="sidebar">
            <div class="sidebar-header">
                <img src="{{asset('picture/dashboard-header.png')}}" alt="" class="headerpic">
                <div>Anggerik Apartment Management System</div>
            </div>

            <ul class="list-unstyled components">
                <table class="user-data">
                    <tr>
                        <td><i class="fas fa-user-circle fa-3x"></i></td>
                        <td><a href="{{url('/officer-profile')}}"><span style="margin-left:2%">Welcome,
                                    {{$user->username}}</span></a></td>
                    </tr>
                </table>
                <!-- <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
              <li>
                <a href="#">Home 1</a>
              </li>
              <li>
                <a href="#">Home 2</a>
              </li>
              <li>
                <a href="#">Home 3</a>
              </li>
            </ul>
          </li> -->
                <li class="{{ (request()->routeIs('officer_home')) ? 'active' : '' }}">
                    <!-- request()->routeIs() is used to check link and if is that specified link, the class is have 'active'-->
                    <a href="{{url('/officer-home')}}">Home</a>
                </li>
                <li class="{{ (request()->routeIs('officer_showOwner','officer_addOwner','officer_editOwner')) ? 'active' : '' }}">
                    <a href="#ownerSubMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">Owner</a>
                    <ul class="collapse list-unstyled" id="ownerSubMenu">
                        <li>
                            <a href="{{url('/officer-show-owner')}}">List of Owner</a>
                        </li>
                        <li>
                            <a href="{{url('/officer-add-owner')}}">Add Owner</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ (request()->routeIs('officer_showUnit','officer_addUnit','officer_detailUnit')) ? 'active' : '' }}">
                    <a href="#unitSubMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">Unit</a>
                    <ul class="collapse list-unstyled" id="unitSubMenu">
                        <li>
                            <a href="{{url('/officer-show-unit')}}">List of Unit</a>
                        </li>
                        <li>
                            <a href="{{url('/officer-add-unit')}}">Add Unit</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!-- Navigation bar start -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-bars"></i>
                    </button>

                    <div class="centered-nav">
                        <span>Anggerik Apartment Management System</span>
                    </div>

                    <div>
                        <ul class="nav navbar-nav ml-auto">
                            <a href="{{url('/request-logout')}}" class="logout-button">Logout</a>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- Navigation bar end -->

            @yield('content')
        </div>
    </div>

    </div>


    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });

    </script>
    @yield('custom_js')
</body>

</html>
