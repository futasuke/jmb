@extends('officer.layout.master')
@section('title','Home')

@section('custom_css')
<link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_detailUnit', $unitId) }}
  </div>

  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <!-- This is content -->
  <div id="inner-content">
    <h1>Unit [{{$unit->number}}] 's Detail</h1>

    <table class="profile-table">
      <tr>
        <td><span>Unit Number: </span></td>
        <td><span>{{$unit->number}}</span></td>
      </tr>
      <tr>
        <td><span>Level: </span></td>
        <td><span>{{$unit->level}}</span></td>
      </tr>
      <tr>
        <td><span>Block: </span></td>
        <td><span>{{$unit->block}}</span></td>
      </tr>
      <tr>
        <td><span>Status: </span></td>
        <td><span>{{$unit->status}}</span></td>
      </tr>
      @if(!empty($unit->user))
      <tr>
        <td><span>Owner: </span></td>
        <td><span>{{$unit->User->username}}</span></td>
      </tr>
      @else
      <tr>
        <td><span>Owner: </span></td>
        <td><span>None</span></td>
      </tr>
      @endif
      @if(!empty($unit->tenant))
      <tr>
        <td><span>Tenant: </span></td>
        <td><span>{{$unit->tenant->fname}} {{$unit->tenant->lname}}</span></td>
      </tr>
      @else
      <tr>
        <td><span>Tenant: </span></td>
        <td><span>None</span></td>
      </tr>
      @endif
      <tr> 
        <td>
          <a href="{{route('officer_requestUpdateUnit',$unit->id)}}" class="submit-button">Edit</a>
        </td>
      </tr>
    </table>
  </div>
@endsection

@section('custom_js')
@endsection
