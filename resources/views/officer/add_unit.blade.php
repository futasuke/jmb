@extends('officer.layout.master')
@section('title','Add Unit')

@section('custom_css')
<link href = {{ asset("css/form_unit/addunitform.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_addUnit') }}
  </div>

  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <!-- This is content -->
  <div id="inner-content">
    <h1>Add Unit</h1>
    <div class="form-card">
      <form method="post" action="{{url('/officer-add-unit')}}">
        @csrf
        <div>
          <table class="add-unit-table">
              <tr>
                <td><span style="font-weight:bold;">Unit Block</span></td>
                <td><select name="block">
                      <option value="A">Block A</option>
                      <option value="B">Block B</option>
                      <option value="C">Block C</option>
                    </select>
                </td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Unit Level</span></td>
                <td><select name="level">
                      <option value=1>Level 1</option>
                      <option value=2>Level 2</option>
                      <option value=3>Level 3</option>
                    </select>
                </td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Unit Number</span></td>
                <td><input type="text" name="unitnumber">&nbsp;<span>*unit number format : A-1-01 // [unit block] - [unit level] - [unit number]</span></td>
              </tr>
              <tr>
                <td><input type="submit" value="Submit" class="submit-button"></td>
              </tr>
          </table>   
        </div>
      </form>
    </div>
  </div>
@endsection

@section('custom_js')
@endsection
