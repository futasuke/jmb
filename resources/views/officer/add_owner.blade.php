@extends('officer.layout.master')
@section('title','Add Owner')

@section('custom_css')
<link href = {{ asset("css/form_unit/addunitform.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_addOwner') }}
  </div>

  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <!-- This is content -->
  <div id="inner-content">
    <h1>Add Owner</h1>
    <div class="form-card">
      <form method="post" action="{{url('/officer-add-owner')}}">
        @csrf
        <div>
          <table class="add-unit-table">
              <tr>
                <td><span style="font-weight:bold;">Username</span></td>
                <td><input type="text" name="username"></td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Fullname</span></td>
                <td><input type="text" name="fullname"></td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Email</span></td>
                <td><input type="text" name="email"><br></td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Select Unit</span></td>
                <td><select name="unit">
                      @foreach($unit as $u)
                        <option value={{$u->number}}>{{$u->number}}</option>
                      @endforeach
                    </select>
                </td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Password</span></td>
                <td><input type="password" name="password"><br></td>
              </tr>
              <tr>
                <td><span style="font-weight:bold;">Retype Password</span></td>
                <td><input type="password" name="password_confirmation"><br></td>
              </tr>
              <tr>
                <td><input type="submit" value="Submit" class="submit-button"></td>
              </tr>
          </table>   
        </div>
      </form>
    </div>
  </div>
@endsection

@section('custom_js')
@endsection
