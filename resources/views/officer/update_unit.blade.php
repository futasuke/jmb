@extends('officer.layout.master')
@section('title','Home')

@section('custom_css')
<link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_detailUnit', $unitId) }}
  </div>

  @if(session()->has('error_message'))
    <div class="add-error-message">
    {!! session('error_message') !!}
    </div>
  @endif

  @if(session()->has('success_message'))
    <div class="add-success-message">
    {!! session('success_message') !!}
    </div>
  @endif

  @if ($errors->any())
      <div class="add-error-message">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
  @endif

  <!-- This is content -->
  <div id="inner-content">
    <h1>Unit [{{$unit->number}}] 's Detail</h1>

    <form method="post" action="">
        @csrf
        <table class="profile-table">
        <tr>
            <td><span>Unit Number: </span></td>
            <td><input type="text" value="{{$unit->number}}" name="number"> &nbsp;<span style="font-size:16;">*unit number format : A-1-01 // [unit block] - [unit level] - [unit number]</span></td>
        </tr>
        <tr>
            <td><span>Level: </span></td>
            <td><input type="text" value="{{$unit->level}}" name="level"></td>
        </tr>
        <tr>
            <td><span>Block: </span></td>
            <td><input type="text" value="{{$unit->block}}" name="block"></td>
        </tr>
        <tr>
            <td><span>Status: </span></td>
            <td><select name="status" id="statusSelect">
                    <option value="Empty">Empty</option>
                    <option value="Own Stay">Own Stay</option>
                    <option value="Rented">Rented</option>
                </select></td>
        </tr>
        <tr>
            <td><span>Owner: </span></td>
            <td><select name="letowner">
                    <option value="stay">No change</option>
                    <option value="removeOWner">Remove owner</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><span>Tenant: </span></td>
            <td><select name="lettenant">
                    <option value="stay">No change</option>
                    <option value="removeTenant">Remove tenant</option>
                </select>
            </td>
        </tr>        
        </table>
        <div class="button-container">
            <a href="{{route('officer_detailUnit',$unit->id)}}" class="submit-button">Cancel</a>
            <input type="submit" class="submit-button" value="Update">
        </div>
    </form>
  </div>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
  if("{{$unit->status}}" == "Empty"){
    document.getElementById("statusSelect").selectedIndex = "0";
  }
  if("{{$unit->status}}" == "Own Stay"){
    document.getElementById("statusSelect").selectedIndex = "1";
  }
  if("{{$unit->status}}" == "Rented"){
    document.getElementById("statusSelect").selectedIndex = "2";
  }
});
</script>
@endsection
