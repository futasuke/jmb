@extends('owner.layout.master')
@section('title','Edit Unit')

@section('custom_css')
<link href = {{ asset("css/form_unit/addunitform.css") }} rel="stylesheet" />
@endsection

@section('content')
    <div>
        {{ Breadcrumbs::render('owner_addTenant') }}
    </div>

    @if(session()->has('error_message'))
        <div class="add-error-message">
        {!! session('error_message') !!}
        </div>
    @endif

    @if(session()->has('success_message'))
        <div class="add-success-message">
        {!! session('success_message') !!}
        </div>
    @endif

    @if ($errors->any())
        <div class="add-error-message">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

    <div id="inner-content">
        <h1 style="margin-bottom:50px">Add New Tenant</h1>
        <div class="form-card">
            <form method="post" action="{{url('/owner-add-tenant')}}">
                @csrf
                <div>
                <table class="add-unit-table">
                    <tr>
                        <td><span style="font-weight:bold;">First Name: </span></td>
                        <td><input type="text" name="fname"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">Last Name: </span></td>
                        <td><input type="text" name="lname"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">IC: </span></td>
                        <td><input type="integer" name="ic"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">Citizenship: </span></td>
                        <td><input type="text" name="citizenship"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">Phone: </span></td>
                        <td><input type="integer" name="phone"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">Duration (Month): </span></td>
                        <td><input type="integer" name="duration"></td>
                    </tr>
                    <tr>
                        <td><span style="font-weight:bold;">Unit Number</span></td>
                        <td><select name="unitNumber">
                                @foreach($unit as $u)
                                    <option value="{{$u->id}}">{{$u->number}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit" class="submit-button"></td>
                    </tr>
                </table>   
                </div>
            </form>
        </div>
    </div>
@endsection

@section('custom_js')
@endsection