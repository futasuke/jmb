@extends('owner.layout.master')
@section('title','Unit List')

@section('custom_css')
<link href = {{ asset("css/datatable/showDataTable.css") }} rel="stylesheet" />
@endsection

@section('content')
<!-- This is breadcrumb -->
<div>
    {{ Breadcrumbs::render('owner_showUnit') }}
</div>

@if(session()->has('error_message'))
        <div class="add-error-message">
        {!! session('error_message') !!}
        </div>
    @endif

    @if(session()->has('success_message'))
        <div class="add-success-message">
        {!! session('success_message') !!}
        </div>
    @endif

    @if ($errors->any())
        <div class="add-error-message">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

<div id="inner-content">
    <h1 style="margin-bottom:50px">Owned Unit List</h1>

    <table id="unit" class="display">
        <thead>
            <tr>
                <th>Unit Number</th>
                <th>Level</th> 
                <th>Block</th>
                <th>Status</th>
                <th>Tenant</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#unit').DataTable({
      serverSide: true,
      ajax: "{{ route('data_owner_unit') }}",
      columns: [
        { name: 'number' },
        { name: 'level' },
        { name: 'block' },
        { name: 'status' },
        { name: 'tenant.fname' },
      ],
    });
} );
</script>
@endsection
