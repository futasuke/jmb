@extends('owner.layout.master')
@section('title','Edit Unit')

@section('custom_css')
<link href = {{ asset("css/profile/profile.css") }} rel="stylesheet" />
@endsection

@section('content')
    <div>
        {{ Breadcrumbs::render('owner_editTenant', $id) }}
    </div>

    @if(session()->has('error_message'))
        <div class="add-error-message">
        {!! session('error_message') !!}
        </div>
    @endif

    @if(session()->has('success_message'))
        <div class="add-success-message">
        {!! session('success_message') !!}
        </div>
    @endif

    @if ($errors->any())
        <div class="add-error-message">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

    <div id="inner-content">
        <h1 style="margin-bottom:50px">Edit Tenant : {{$tenant->fname}} {{$tenant->lname}}</h1>

        <form method="post" action="{{route('owner_updateTenant',$tenant->id)}}">
            @csrf
            <table class="profile-table">
                <tr>
                    <td><span>First Name: </span></td>
                    <td><input type="text" value="{{$tenant->fname}}" name="fname"></td>
                </tr>
                <tr>
                    <td><span>Last Name: </span></td>
                    <td><input type="text" value="{{$tenant->lname}}" name="lname"></td>
                </tr>
                <tr>
                    <td><span>IC: </span></td>
                    <td><input type="text" value="{{$tenant->ic}}" name="ic"></td>
                </tr>
                <tr>
                    <td><span>Citizenship: </span></td>
                    <td><input type="text" value="{{$tenant->citizenship}}" name="citizenship"></td>
                </tr>
                <tr>
                    <td><span>Phone Number: </span></td>
                    <td><input type="text" value="{{$tenant->phone}}" name="phone"></td>
                </tr>
                <tr>
                    <td><span>Staying Duration (Month): </span></td>
                    <td><input type="text" value="{{$tenant->duration}}" name="duration"></td>
                </tr>    
            </table>
            <div class="button-container">
                <a href="{{route('owner_showTenant')}}" class="submit-button">Cancel</a>
                <input type="submit" class="submit-button" value="Update">
            </div>
        </form>
    </div>
@endsection

@section('custom_js')
@endsection