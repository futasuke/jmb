@extends('owner.layout.master')
@section('title','Home')

@section('custom_css')
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('officer_home') }}
  </div>

  @if(session()->has('error_message'))
        <div class="add-error-message">
        {!! session('error_message') !!}
        </div>
    @endif

    @if(session()->has('success_message'))
        <div class="add-success-message">
        {!! session('success_message') !!}
        </div>
    @endif

    @if ($errors->any())
        <div class="add-error-message">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

  <div id="inner-content">
    <h1>Home Section</h1>
  </div>
@endsection

@section('custom_js')
@endsection
