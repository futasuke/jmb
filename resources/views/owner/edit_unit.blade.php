@extends('owner.layout.master')
@section('title','Edit Unit')

@section('custom_css')
@endsection

@section('content')
<div>
{{ Breadcrumbs::render('owner_editUnit', $id) }}
</div>

<div id="inner-content">
    <h1 style="margin-bottom:50px">Edit Unit [{{$unit->number}}]</h1>
</div>
@endsection

@section('custom_js')
@endsection