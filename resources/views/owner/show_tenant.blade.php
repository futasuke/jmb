@extends('owner.layout.master')
@section('title','Tenant List')

@section('custom_css')
<link href = {{ asset("css/datatable/showDataTable.css") }} rel="stylesheet" />
@endsection

@section('content')
  <!-- This is breadcrumb -->
  <div>
    {{ Breadcrumbs::render('owner_showTenant') }}
  </div>

  @if(session()->has('error_message'))
        <div class="add-error-message">
        {!! session('error_message') !!}
        </div>
    @endif

    @if(session()->has('success_message'))
        <div class="add-success-message">
        {!! session('success_message') !!}
        </div>
    @endif

    @if ($errors->any())
        <div class="add-error-message">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

  <div id="inner-content">
    <h1 style="margin-bottom:50px">Tenant List Section</h1>

    <table id="tenant" class="display">
        <thead>
            <tr>
                <th>Unit Number</th>
                <th>First Name</th>
                <th>Last Name</th> 
                <th>IC</th>
                <th>Citizenship</th>
                <th>Phone</th>
                <th>Stay Duration (month)</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Delete</h4>
          </div>
          <div class="modal-body">
            <p style="color:black;">Are you sure you want to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="submit-button" data-dismiss="modal">Close</button>
            <form method="POST" action="" style="margin:0px;" id="deleteForm">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input type="submit" class="submit-button" value="Delete">
            </form>
          </div>
        </div>
        
      </div>
  </div>
  <!-- Modal End -->
  </div>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#tenant').DataTable({
      serverSide: true,
      ajax: "{{ route('data_owner_tenant') }}",
      columns: [
        { data:'number',name: 'number'},
        { data:'tenant.fname',name: 'tenant.fname'},
        { data:'tenant.lname',name: 'tenant.lname'},
        { data:'tenant.ic',name: 'tenant.ic'},
        { data:'tenant.citizenship',name: 'tenant.citizenship'},
        { data:'tenant.phone',name: 'tenant.phone'},
        { data:'tenant.duration',name: 'tenant.duration'},
        { data:'tenant.id',name: 'tenant.id' },
      ],
      columnDefs: [
        {
          targets: 7,
          "data": 'id',
          render: function ( data, type, row, meta ) {
            return '<a href="edit-owner-tenant/'+data+'" class="far fa-edit submit-button"></a> &nbsp; <a href="#" data-toggle="modal" data-target="#myModal" onclick="deleteData('+data+')" class="far fa-trash-alt submit-button"></a>';
          }
        }
      ],
    });
} );

function deleteData(id){
  var id = id;
  var url = '{{url("/owner-delete-tenant")}}/'+id;

  $('#deleteForm').attr('action',url);
}
</script>
@endsection
