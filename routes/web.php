<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::get('/', 'HomeController@loginForm')->name('home');
Route::get('/request-logout', 'HomeController@requestLogout');
Route::post('/request-login', 'HomeController@requestLogin');


//Officer
Route::middleware(['auth'])->group(function () {
    Route::get('/officer-home', 'OfficerController@home')->name('officer_home');
    Route::get('/officer-profile', 'OfficerController@profile')->name('officer_profile');
    Route::get('/officer-profile-edit', 'OfficerController@editProfile')->name('officer_edit_profile');
    Route::post('/officer-profile-update', 'OfficerController@updateProfile')->name('officer_update_profile');

    Route::get('/officer-show-owner', 'OfficerController@showOwner')->name('officer_showOwner');
    Route::get('/officer-add-owner', 'OfficerController@addOwner')->name('officer_addOwner');
    Route::get('/edit-officer-owner/{ownerId}', 'OfficerController@editOwner')->name('officer_editOwner');
    Route::post('/officer-add-owner', 'OfficerController@requestAddOwner')->name('officer_requestAddOwner');
    Route::get('/officer-update-owner/{ownerId}', 'OfficerController@updateOWner')->name('officer_updateOwner');
    Route::post('/officer-update-owner/{ownerId}', 'OfficerController@editOwnerDetail')->name('officer_requestUpdateOwner');

    Route::get('/officer-show-unit', 'OfficerController@showUnit')->name('officer_showUnit');
    Route::get('/officer-add-unit', 'OfficerController@addUnit')->name('officer_addUnit');
    Route::get('/officer-detail-unit/{unitId}', 'OfficerController@detailUnit')->name('officer_detailUnit');
    Route::post('/officer-add-unit', 'OfficerController@requestAddUnit')->name('officer_requestAddUnit');
    Route::get('/officer-update-unit/{unitId}', 'OfficerController@updateUnit')->name('officer_updateUnit');
    Route::post('/officer-update-unit/{unitId}', 'OfficerController@requestUpdateUnit')->name('officer_requestUpdateUnit');

    Route::delete('/officer-delete-owner/{id}', 'OfficerController@deleteOwner')->name('officer_deleteOwner');
    Route::delete('/officer-delete-unit/{id}', 'OfficerController@deleteUnit')->name('officer_deleteUnit');
});

//Owner
Route::middleware(['auth'])->group(function () {
    Route::get('/owner-home', 'OwnerController@home')->name('owner_home');
    Route::get('/owner-profile', 'OwnerController@profile')->name('owner_profile');
    Route::get('/owner-profile-edit', 'OwnerController@editProfile')->name('owner_edit_profile');
    Route::post('/owner-profile-update', 'OwnerController@updateProfile')->name('owner_update_profile');

    Route::get('/owner-show-tenant', 'OwnerController@showTenant')->name('owner_showTenant');
    Route::get('/owner-show-unit', 'OwnerController@showUnit')->name('owner_showUnit');
    Route::get('/edit-owner-unit/{id}', 'OwnerController@editUnit')->name('owner_editUnit');

    Route::get('/edit-owner-tenant/{id}', 'OwnerController@editTenant')->name('owner_editTenant');
    Route::get('/owner-add-tenant', 'OwnerController@addTenant')->name('owner_addTenant');
    Route::post('/owner-update-tenant/{tenantId}', 'OwnerController@editTenantDetail')->name('owner_updateTenant');
    Route::delete('/owner-delete-tenant/{id}', 'OwnerController@deleteTenant')->name('owner_deleteTenant');
    Route::post('/owner-add-tenant', 'OwnerController@requestAddTenant')->name('owner_requestAddTenant');
});

//Datatables
Route::get('/data-owner-unit', 'OwnerController@getUnit')->name('data_owner_unit');
Route::get('/data-owner-tenant', 'OwnerController@getTenant')->name('data_owner_tenant');

Route::get('/data-officer-unit/{block}', 'OfficerController@getUnit')->name('data_officer_unit');
Route::get('/data-officer-owner', 'OfficerController@getOwner')->name('data_officer_owner');
Route::get('/data-officer-tenant/{ownerId}', 'OfficerController@getTenant')->name('data_officer_tenant');

//Test purpose
Route::get('/test1', 'OfficerController@test');
Route::get('/testAuth', 'HomeController@authCheck');
Route::get('/test2', 'TestController@wehawt');
