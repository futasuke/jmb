<?php

// OFFICER breadcrumb START
Breadcrumbs::for('officer_home', function ($trail) {
    $trail->push('Home', route('officer_home'));
});

Breadcrumbs::for('officer_profile', function ($trail) {
    $trail->push('Profile', route('officer_profile'));
});

Breadcrumbs::for('officer_edit_profile', function ($trail) {
    $trail->parent('officer_profile');
    $trail->push('Edit Profile', route('officer_edit_profile'));
});

Breadcrumbs::for('officer_showOwner', function ($trail) {
    $trail->parent('officer_home');
    $trail->push('Owner List', route('officer_showOwner'));
});

Breadcrumbs::for('officer_showUnit', function ($trail) {
    $trail->parent('officer_home');
    $trail->push('Unit List', route('officer_showUnit'));
});

Breadcrumbs::for('officer_addUnit', function ($trail) {
    $trail->parent('officer_showUnit');
    $trail->push('Add Unit', route('officer_addUnit'));
});

Breadcrumbs::for('officer_addOwner', function ($trail) {
    $trail->parent('officer_showOwner');
    $trail->push('Add Owner', route('officer_addOwner'));
});

Breadcrumbs::for('officer_editOwner', function ($trail, $ownerId) {
    $trail->parent('officer_showOwner');
    $trail->push('Owner Detail', route('officer_editOwner', $ownerId));
});

Breadcrumbs::for('officer_detailUnit', function ($trail, $unitId) {
    $trail->parent('officer_showUnit');
    $trail->push('Unit Detail', route('officer_detailUnit', $unitId));
});
// OFFICER breacrumbs END

// OWNER breadcrumbs
Breadcrumbs::for('owner_home', function ($trail) {
    $trail->push('Home', route('owner_home'));
});

Breadcrumbs::for('owner_profile', function ($trail) {
    $trail->push('Profile', route('owner_profile'));
});

Breadcrumbs::for('owner_edit_profile', function ($trail) {
    $trail->parent('owner_profile');
    $trail->push('Edit Profile', route('owner_edit_profile'));
});

Breadcrumbs::for('owner_showTenant', function ($trail) {
    $trail->parent('owner_home');
    $trail->push('Tenant List', route('owner_showTenant'));
});

Breadcrumbs::for('owner_showUnit', function ($trail) {
    $trail->parent('owner_home');
    $trail->push('Unit List', route('owner_showUnit'));
});

// Breadcrumbs::for('owner_editUnit', function ($trail) {
//     $trail->parent('owner_showUnit');
//     $trail->push('Unit List', route('owner_editUnit'));
// });

Breadcrumbs::for('owner_editUnit', function ($trail, $id) {
    $trail->parent('owner_showUnit');
    $trail->push('Edit Unit', route('owner_editUnit',$id));
});

Breadcrumbs::for('owner_editTenant', function ($trail, $id) {
    $trail->parent('owner_showTenant');
    $trail->push('Edit Tenant', route('owner_editTenant',$id));
});

Breadcrumbs::for('owner_addTenant', function ($trail) {
    $trail->parent('owner_showTenant');
    $trail->push('Add Tenant', route('owner_addTenant'));
});

// // Home > About
// Breadcrumbs::for('about', function ($trail) {
//     $trail->parent('home');
//     $trail->push('About', route('about'));
// });

// // Home > Blog
// Breadcrumbs::for('blog', function ($trail) {
//     $trail->parent('home');
//     $trail->push('Blog', route('blog'));
// });

// // Home > Blog > [Category]
// Breadcrumbs::for('category', function ($trail, $category) {
//     $trail->parent('blog');
//     $trail->push($category->title, route('category', $category->id));
// });

// // Home > Blog > [Category] > [Post]
// Breadcrumbs::for('post', function ($trail, $post) {
//     $trail->parent('category', $post->category);
//     $trail->push($post->title, route('post', $post->id));
// });