<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    //
    protected $table = 'units';
    use SoftDeletes;

    public function user()
    {
      return $this->belongsTo('App\User', 'owner_id', 'id');
    }

    public function tenant(){
      return $this->belongsTo('App\Tenant', 'tenant_id', 'id');
    }
}
