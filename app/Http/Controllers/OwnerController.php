<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserInfo;
use App\Unit;
use App\Tenant;
use Freshbitsweb\Laratables\Laratables;
use Yajra\Datatables\Datatables;

class OwnerController extends Controller
{
    //
    public function home()
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
          return redirect()->back(); //redirect to previous page if user does not have rights to
        }
  
        return view('owner.home', compact('user'));
    }
  
    public function profile()
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }
  
        return view('owner.profile', compact('user'));
    }
  
    public function editProfile()
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }
        
        return view('owner.profile_edit', compact('user'));
    }
  
    public function updateProfile(Request $request)
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        $userInfo = UserInfo::where('user_id', $id)->first();
  
        $userInfo->fullname = $request->input('fullname');
        $userInfo->phone = $request->input('phone');
        $userInfo->address = $request->input('address');
        $user->userinfo()->save($userInfo);
  
        return redirect()->route('owner_profile')->with('success_message', 'Profile successfully updated.');
    }
  
    public function showTenant()
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }
  
        return view('owner.show_tenant', compact('user'));
    }
  
    public function showUnit()
    {
        $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $id)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }
  
        return view('owner.show_unit', compact('user'));
    }
  
    public function getUnit(){
        return Laratables::recordsOf(Unit::class, function ($query) {
            return $query->where('owner_id', Auth::guard('user')->id());
        });

        // $data = Unit::all();
        // return response()->json($data);
    }
    
    public function editUnit($id){
        $userID = Auth::guard('user')->id();//get User ID based on login session using Auth::guard
        $user = User::where('id', $userID)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }

        $unit = Unit::where('id',$id)->where('owner_id',$userID)->first();       
        if(empty($unit)){
            return redirect()->back()->with('error_message','You do not have permission to do that.');
        }
        return view('owner.edit_unit', compact('user','unit','id'));
    }

    public function getTenant(){
        $unit = Unit::with('Tenant')->where('owner_id', Auth::guard('user')->id())->where('tenant_id','!=',null)->get();
        return Datatables::of($unit)->make(true);

        // return Laratables::recordsOf(Unit::class, function ($query) {
        //     return $query->where('owner_id', Auth::guard('user')->id());
        // });
    }

    public function addTenant(){
        $userID = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $userID)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }

        $unit = Unit::where('status','!=','Rented')->where('owner_id',$user->id)->get();

        return view('owner.add_tenant', compact('user','unit'));
    }

    public function editTenant($id){
        $userID = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
        $user = User::where('id', $userID)->first();
        if ($user->role != 'owner') { //check user role for accessing the view.
            return redirect()->back();
        }

        $tenant = Tenant::where('id', $id)->first();
        return view('owner.edit_tenant', compact('user', 'tenant', 'id'));
    }

    public function requestAddTenant(Request $request){       
        $tenant = new Tenant;
        $tenant->fname = $request->input('fname');
        $tenant->lname = $request->input('lname');
        $tenant->ic = $request->input("ic");
        $tenant->citizenship = $request->input("citizenship");
        $tenant->phone = $request->input("phone");
        $tenant->duration = $request->input("duration");
        $tenant->save();

        $unit = Unit::where('id',$request->input('unitNumber'))->first();
        $unit->tenant_id = $tenant->id;
        $unit->status ="Rented";
        $unit->save();

        return redirect()->back()->with('success_message','Tenant successfully added!');
    }

    public function editTenantDetail(Request $request,$tenantId)
    {
      $tenant = Tenant::find($tenantId);
      $tenant ->fname = $request->input("fname");
      $tenant ->lname = $request->input("lname");
      $tenant ->ic = $request->input("ic");
      $tenant ->citizenship = $request->input("citizenship");
      $tenant ->phone = $request->input("phone");
      $tenant ->duration = $request->input("duration");

      $tenant->save();

      return redirect()->back()->with('success_message','Tenant detail successfully edited!');

    }

    public function deleteTenant($id)
    {
        $tenant = tenant::findOrFail($id);
        $tenant->unit->tenant_id = null;
        $tenant->unit->status = "Own Stay";
        $tenant->unit->save();
        $tenant->delete();
        return redirect()->back()->with('success_message','Tenant successfully deleted!');     
    }

    public function test()
    {
        $user = User::with('UserInfo')->get();
        dd($user);
    }
}
