<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserInfo;
use App\Unit;
use App\Tenant;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;
use Yajra\Datatables\Datatables;

class OfficerController extends Controller
{
    public function home(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back(); //redirect to previous page if user does not have rights to
      }

      return view('officer.home', compact('user'));
    }

    public function profile(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      return view('officer.profile', compact('user'));
    }

    public function editProfile()
    {
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }
      
      return view('officer.profile_edit', compact('user'));
    }

    public function updateProfile(Request $request){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      $userInfo = UserInfo::where('user_id', $id)->first();

      $userInfo->fullname = $request->input('fullname');
      $userInfo->phone = $request->input('phone');
      $userInfo->address = $request->input('address');
      $user->userinfo()->save($userInfo);

      return redirect()->route('officer_profile')->with('success_message', 'Profile successfully updated.');
    }

    public function showOwner(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      return view('officer.show_owner', compact('user'));
    }

    public function getOwner(){
      $owner = User::with('UserInfo','Unit')->where('role','owner')->get();

      return Datatables::of($owner)->make(true);

      // return Laratables::recordsOf(User::class, function ($query){
      //   return $query->where('role','owner');
      // });
    }

    public function addOwner(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      $unit = Unit::where('status', 'empty')->get();

      return view('officer.add_owner', compact('user','unit'));
    }

    public function editOwner($ownerId){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      $owner = User::where('id',$ownerId)->where('role','owner')->first();
      if(empty($owner)){
        return redirect()->back()->with('error_message','Id does not exist or you do not have permission to do that.');
      }

      return view('officer.show_owner_detail', compact('user','owner','ownerId'));
    }

    public function updateOWner($ownerId){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      $owner = User::where('id',$ownerId)->where('role','owner')->first();
      if(empty($owner)){
        return redirect()->back()->with('error_message','Id does not exist or you do not have permission to do that.');
      }

      return view('officer.update_owner', compact('user','owner'));
    }

    public function requestAddOwner(Request $request){
      $checkEmail = User::where('email',$request->input('email'))->first(); //check if email already exist or not
      if(!empty($checkEmail)){
        return redirect()->back()->with('error_message', 'Email already exist!');
      }

      $validatedData = $request->validate([
        'email' => 'required|email',
        'password' => 'required|same:password_confirmation',
      ]);

      $user = new User;
      $user->username = $request->input('username');
      $user->email = $request->input('email');
      $user->role = 'owner';
      $user->password = Hash::make($request->input('password'));
      $user->save();

      $userSlot = User::where('email',$request->input('email'))->first();
      // dd($userSlot);
      $userInfo = new UserInfo;
      $userInfo->address = 'Somewhere';
      $userInfo->fullname = $request->input('fullname');
      $userSlot->userinfo()->save($userInfo);

      $unit = Unit::where('number', $request->input('unit'))->first();
      $unit->status = "Own Stay";
      $unit->save();
      $userSlot->unit()->save($unit);

      return redirect()->back()->with('success_message','Owner successfully added!');
    }

    public function showUnit(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      return view('officer.show_unit', compact('user'));
    }

    public function getUnit($block){
      if($block=='blockA'){
        return Laratables::recordsOf(Unit::class,function($query){
          return $query->where('block','A');
        });
      }
      if ($block=='blockB') {
        return Laratables::recordsOf(Unit::class,function($query){
          return $query->where('block','B');
        });
      }
      if ($block=='blockC') {
        return Laratables::recordsOf(Unit::class,function($query){
          return $query->where('block','C');
        });
      }
    }
    
    public function addUnit(){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      return view('officer.add_unit', compact('user'));
    }

    public function detailUnit($unitId){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      $unit = Unit::where('id',$unitId)->first();

      return view('officer.show_unit_detail', compact('user','unit','unitId'));
    }

    public function requestAddUnit(Request $request){
      $validatedData = $request->validate([
        'unitnumber' => 'regex:/^([A-Z])-[0-9]-[0-9][0-9]$/',
      ]);

      $checkUnit = Unit::where('number', $request->input('unitnumber'))->first(); //Check if unit number already exist
      if(!empty($checkUnit)){
        return redirect()->back()->with('error_message','Unit already exist!');
      }

      $unit = new Unit;
      $unit->owner_id = null;
      $unit->tenant_id = null;
      $unit->status = "Empty";
      $unit->number = $request->input('unitnumber');
      $unit->level = $request->input('level');
      $unit->block = $request->input('block');

      $unit->save();

      return redirect()->back()->with('success_message', 'Unit successfully added!');
    }

    public function updateUnit($unitId){
      $id = Auth::guard('user')->id(); //get User ID based on login session using Auth::guard
      $user = User::where('id', $id)->first();
      if($user->role != 'officer'){ //check user role for accessing the view.
        return redirect()->back();
      }

      $unit = Unit::where('id',$unitId)->first();

      return view('officer.update_unit', compact('user','unit','unitId'));
    }

    public function editOwnerDetail(Request $request, $ownerId)
    {
      $owner = User::where('id',$ownerId)->where('role','owner')->first();

      $checkEmail = User::where('email',$request->input('email'))->first(); // check if email already exist if they want to change.
      if(!empty($checkEmail) && $request->input('email')!= $owner->email){ //if checkEmail is not empty AND the email is not the same as the email that is currently used.
        return redirect()->back()->with('error_message', 'Email already exist!');
      }
      // dd($request->input('username'));
      $owner->update([
        'username' => $request->input('username'),
        'email' => $request->input('email'),  
      ]);

      $owner->userinfo()->update([
        'fullname'=>$request->input('fullname'),
        'phone'=>$request->input('phone'),
        'address'=>$request->input('address'),
      ]);

      // $owner->UserInfo->fullname = $request->input("fullname");
      // $owner->username = $request->input("username");
      // $owner->UserInfo->phone = $request->input("phone");
      // $owner->UserInfo->address = $request->input("address");
      // // dd($owner);
      // $owner->userinfo->save();

      return redirect()->route('officer_editOwner',$ownerId)->with('success_message','Owner successfully edited!');

    }
  
    public function deleteUnit($id)
    {
        $unit = Unit::findOrFail($id);
        $unitNum = $unit->number;
        $unit->delete();
        return redirect()->back()->with('success_message','Unit '.$unitNum.' successfully delete!');
    }
    public function deleteOwner($id)
    {
      $owner = User::findOrFail($id);
      $unit = Unit::where('owner_id',$id)->get();
      foreach($unit as $u){
        $u->owner_id = null;
        $u->tenant_id = null;
        $u->status = 'Empty';
        $u->save();
      }
      $owner->userinfo()->delete();
      $owner->delete();

      return redirect()->back()->with('success_message','Owner successfully delete!');
    }

    public function requestUpdateUnit(Request $request,$unitId){
      $validatedData = $request->validate([ //Check if the number follows the format or not
        'number' => 'regex:/^([A-Z])-[0-9]-[0-9][0-9]$/',
      ]);

      $checkUnit = Unit::where('number', $request->input('number'))->first(); //Check if unit number already exist
      if(!empty($checkUnit) && $checkUnit->number!=$request->input('number')){
        return redirect()->back()->with('error_message','Unit already exist!');
      }

      $unit = Unit::where('id',$unitId)->first();
      $unit->number = $request->input("number");
      $unit->level = $request->input("level");
      $unit->block = $request->input("block");
      $unit->status = $request->input("status");
      if($request->input("letowner")=="removeOWner"){
        $unit->owner_id = null;
        $unit->status = "Empty";
      }
      if($request->input("lettenant")=="removeTenant"){
        $unit->tenant_id = null;
      }
      $unit->save();

      return redirect()->route('officer_detailUnit',$unitId)->with('success_message','Unit successfuly updated!');
    }

    public function getTenant($ownerId){;
      $unit = Unit::with('Tenant')->where('owner_id',$ownerId)->where('tenant_id','!=',null)->get();

      return Datatables::of($unit)->make(true);
    }

    public function test(){
      $user = User::with('UserInfo')->get();
      dd($user);
    }
}
