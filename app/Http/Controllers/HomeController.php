<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class HomeController extends Controller
{
    public function loginForm(){
      return view('login');
    }

    public function requestLogin(Request $request){
      $user = User::where('email', $request->input('email'))->first();

      if(!empty($user)){
        if(Hash::check($request->input('password'), $user->password)){
          $userdata = array(
              'email' => $request->input('email') ,
              'password' => $request->input('password'),
          );
          if (Auth::guard('user')->attempt($userdata)){
            if($user->role=="officer"){
              return redirect()->route('officer_home');
            }
            if($user->role=="owner"){
              return redirect()->route('owner_home');
            }
          }
          else{
              echo "fail";
          }
        }
        else{
          return redirect()->route('home')->with('error_message', "The password that you've entered is incorrect.");
        }
      }
      else{
        return redirect()->route('home')->with('error_message', 'Email does not exist!');
      }
    }

    public function requestLogout()
    {
      Auth::logout();
      return view('login');
    }

    public function authCheck(){
      if(Auth::check()){
        echo "login";
      }
      else{
        echo "logout";
      }
    }
}
