<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    //
    protected $table = 'tenants';

    use SoftDeletes;

    public function unit(){
      return $this->hasOne('App\Unit', 'tenant_id', 'id');
    }
}
